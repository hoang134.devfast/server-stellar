'use strict'
const { json } = require('express/lib/response')
// const StellarSdk = require('stellar-sdk')
// const server = new StellarSdk.Server('https://horizon.stellar.org')

var StellarSdk = require("stellar-sdk");
//StellarSdk.default_network = StellarSdk.Networks.PUBLIC;
var server = new StellarSdk.Server("https://horizon.stellar.org");
//server.networkPassphrase = StellarSdk.Networks.PUBLIC;
// const send = async (publicKey, secretKey) => {
//     const account = await server.loadAccount(publicKey);
//     //console.log(account);
//       account.balances.forEach(function (balance) {
//         //console.log("Type:", balance.asset_type, ", Balance:", balance.balance);
//         payments(secretKey, balance.balance  - 1000);
//   });
// }
// const getInforAccountMe = async (publicKey) => {
//   const account = await server.loadAccount(publicKey);
//     account.balances.forEach(function (balance) {
//       console.log("Type:", balance.asset_type, ", Balance:", balance.balance);
// });
// }


const payments = async () => {

}

module.exports = {
  get: async (req, res) => {
    const account = await server.loadAccount(req.params.public_key);
    let amount = 0;
    account.balances.forEach(function (balance) {
      amount = balance.balance
    });
    return res.json({ data: amount });
  },

  post: async (req, res) => {

    let secretKey = req.params.private_key
    let amountCoin = req.params.amount

    const sourceKeys = StellarSdk.Keypair.fromSecret(
      secretKey
    )
    const destinationId = process.env.PUBLICKEY
    // Transaction will hold a built transaction we can resubmit if the result is unknown.
    let transaction

    // First, check to make sure that the destination account exists.
    // You could skip this, but if the account does not exist, you will be charged
    // the transaction fee when the transaction fails.

    await server
      .loadAccount(destinationId)
      // If the account is not found, surface a nicer error message for logging.
      .catch(function (error) {
        if (error instanceof StellarSdk.NotFoundError) {
          res.status(400).json({status: 400, message: "The destination account does not exist!"})
        } else { return error }
      })
      // If there was no error, load up-to-date information on your account.
      .then(function () {
        return server.loadAccount(sourceKeys.publicKey())
      })
      .then(function (sourceAccount) {
        // Start building the transaction.
        transaction = new StellarSdk.TransactionBuilder(sourceAccount, {
          fee: StellarSdk.BASE_FEE,
          networkPassphrase: StellarSdk.Networks.PUBLIC
        })
          .addOperation(
            StellarSdk.Operation.payment({
              destination: destinationId,
              // Because Stellar allows transaction in many currencies, you must
              // specify the asset type. The special "native" asset represents Lumens.
              asset: StellarSdk.Asset.native(),
              amount: amountCoin + ''
            })
          )
          // A memo allows you to add your own metadata to a transaction. It's
          // optional and does not affect how Stellar treats the transaction.
          .addMemo(StellarSdk.Memo.text('Transaction'))
          // Wait a maximum of three minutes for the transaction
          .setTimeout(180)
          .build()
        // Sign the transaction to prove you are actually the person sending it.
        transaction.sign(sourceKeys)
        // And finally, send it off to Stellar!
        return server.submitTransaction(transaction)
      })
      .then(function (result) {
        return res.json({ data: req.params.amount });
      })
      .catch(function (error) {
        console.error('Something went wrong!', error)
        res.status(400).json({status: 400, message: "Something went wrong"})
        // If the result is unknown (no response body, timeout etc.) we simply resubmit
        // already built transaction:
        // server.submitTransaction(transaction);
      })
  }
}