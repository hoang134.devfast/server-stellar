'use strict';

module.exports = function(app) {
  var stellarCtrl = require('./controllers/StellarController');

  app.route('/stellar/payments/:private_key/:amount')
    .post(stellarCtrl.post);

  app.route('/stellar/getAmount/:public_key')
    .get(stellarCtrl.get);
};
